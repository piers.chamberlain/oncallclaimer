import calendar
from UserPayment import UserPayment

class ConsoleOutput:
    def __init__(self, userclaims, schedules, enddate):
        self.userclaims = userclaims
        self.schedules = schedules
        self.enddate = enddate
        self.userpayments = []
        self.process()
    
    def process(self):
        self.combineSchedulePayments()
        self.printWelcome()
        self.printSchedules()
        self.printTotalPayments()
        self.printExit()

    def combineSchedulePayments(self):
        #uniqueusers
        for uu in self.userclaims.getUniqueUsers():
            totalUserPayment = 0
            numberHolsWorked = []
            for scfu in self.userclaims.summarisedClaimsForUser:
                if scfu.pd_user_id == uu[0]:
                    totalUserPayment += scfu.totalScheduleAmount
                    if len(scfu.holidaysWorked) > 0:
                        numberHolsWorked.extend(scfu.holidaysWorked)
            if totalUserPayment > 0 :
                uniqueHols = len(set(numberHolsWorked))
                self.userpayments.append(UserPayment(uu[1], totalUserPayment, uniqueHols))

    def printWelcome(self):
        print("Hi team,")
        print()
        print("The following people have adjustments for this week's pay run, the fortnight ending " + calendar.day_name[self.enddate.weekday()] + ", " + self.enddate.strftime("%Y-%m-%d"))
        print()

    def printExit(self):
        print()
        print("Thanks")
        print()

    def printSchedules(self):
        print("Schedules included:")
        for sched in self.schedules:
            print("    " +sched.schedule_rate.schedule_name)
        print()
    
    def printTotalPayments(self):
        for up in self.userpayments:
            line = up.userName.ljust(19) + (" $"+ str(up.totalPayment) + ".00").ljust(9)
            if up.holidayCount > 0:
                line = line + " and " + str(up.holidayCount) + " alternate day(s)"
            print(line)
    
    def printNewUsers(self):
        print()
        if len([x for x in self.userclaims if x.is_new_user == True]) > 0:
            print("Also the following users appear to be new on paid on-call schedules.")
            print("Please ensure their BYOD Mobile allowance is processed as a contract variation.")

        for uc in self.userclaims:
            if uc.is_new_user == True:
                print("     "+uc.name)