from pdpyras import APISession
from datetime import datetime
from datetime import timedelta
from datetime import date
from datetime import time
import json
import os
from google.cloud import storage
from localpackage.DaylightSavings import DaylightSavings
from localpackage.PartTimers import PartTimers
from localpackage.ScheduleHistory import ScheduleHistory
from localpackage.ScheduleConfig import ScheduleConfig
from localpackage.UserShift import UserShift
from localpackage.UserClaimList import UserClaimList
from localpackage.FormatOutput import FormatOutput
from localpackage.Mailgun import Mailgun


def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    return d + timedelta(days_ahead)

def preprocess(event, context):
    storage_client = storage.Client()
    bucket = storage_client.bucket("oncall-claimer")
    lastExecutionBlob = bucket.blob("lastExecution.json")
    executionDetails = lastExecutionBlob.download_as_string()
    executionDetailContent = json.loads(executionDetails)
    lastRun = datetime.strptime(executionDetailContent['latestExecutionDateTime'], "%Y-%m-%dT%H:%M")
    daysPast = datetime.now() - lastRun
    if ( daysPast.days >= 13 ) :
        print("Last execution date is sufficiently old. Commencing this run.")
        thisRunText = process()
        executionDetailContent['previousExecutionDateTime'] = executionDetailContent['latestExecutionDateTime']
        executionDetailContent['previousExecutionContent'] = executionDetailContent['latestExecutionContent']
        executionDetailContent['latestExecutionDateTime'] = datetime.now().strftime("%Y-%m-%dT%H:%M")
        executionDetailContent['latestExecutionContent'] = thisRunText
        #Write file
        lastExecutionBlob.upload_from_string(json.dumps(executionDetailContent))
    else:
        print("Last execution date is less than a fornight ago. Skipping this run.")

def process():

    next_sunday = next_weekday(date.today(), 6) # 0 = Monday, 1=Tuesday, 2=Wednesday...
    startdate = datetime.combine(next_sunday + timedelta(days= -13), time(hour=0,minute=0))
    historydate = startdate + timedelta(weeks = -13)
    enddate = datetime.combine(next_sunday ,time(hour=23,minute=59))

    # -----------------
    # Configuration load
    storage_client = storage.Client()
    bucket = storage_client.bucket("oncall-claimer")
    configBlob = bucket.blob("config.json")
    contents = configBlob.download_as_string()
    configContent = json.loads(contents)

    schedule_config = ScheduleConfig(configContent['schedules'])
    part_timers = PartTimers(configContent['partTimers'])

    holiday_dates = []
    for holiday in configContent['holidays']:
        holiday_dates.append(datetime.strptime(holiday, "%Y-%m-%d").date())

    #api_token = configContent['pdApiReadOnlyToken']
    api_token = os.environ.get('PAGERDUTY_API_TOKEN')
    session = APISession(api_token)

    ds_helper = DaylightSavings(configContent['daylightSavingsEnd'],configContent['daylightSavingsStart'])
    delta = enddate-startdate 
    days = []
    days.append(startdate)
    days.extend([startdate + timedelta(days=i+1) for i in range(delta.days)])

    # -----------------

    # -----------------
    # Iterate through the schedules, build a set of days, and which users get paid
    scheduleList = []

    for rate in schedule_config.rates:
        sched = session.rget('/schedules/'+ rate.schedule_id, params={    
            'since' : startdate.strftime("%Y-%m-%dT%H:%M:%S"+ds_helper.getTimezone(startdate)), 
            'until' : enddate.strftime("%Y-%m-%dT%H:%M:%S"+ds_helper.getTimezone(enddate)) })
        
        thisSchedule = ScheduleHistory(rate, days, holiday_dates, part_timers)

        #Build a UserShift array - a User can have muliple shifts on one or more rosters in the period
        for entry in sched['final_schedule']['rendered_schedule_entries']:
            pd_user_shift = UserShift(sched['id'], entry['user']['summary'], entry['user']['id'], entry['start'], entry['end'])
            thisSchedule.all_schedule_users.append((entry['user']['id'], entry['user']['summary']))
            thisSchedule.pd_shifts.append(pd_user_shift)
            thisSchedule.shift_pieces.extend(pd_user_shift.getShiftPieces())

        thisSchedule.processScheduleShifts()
        scheduleList.append(thisSchedule)
    # -----------------

    # -----------------
    # Summarise the amounts per user and per schedule
    claim_list = UserClaimList(scheduleList)
    # Sum claims
    claim_list.combineAllUserClaims()
    # -----------------

    # -----------------=
    # Summary / write to console and emails
    format = FormatOutput(claim_list, scheduleList, enddate)
    text = format.process()
    toList = []
    for email in configContent['emailToList']:
        toList.append(email['Address'])    
    ccList = []
    for ccEmail in configContent['emailCcList']:
        ccList.append(ccEmail['Address'])
    mail = Mailgun( configContent['mailgunDomain'], text, toList, ccList)
    emailApiResponse = mail.send()
    print(emailApiResponse)
    # -----------------

    return text

#Uncomment for local debugging: 
#preprocess("a","b")