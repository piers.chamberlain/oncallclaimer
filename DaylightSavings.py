from datetime import datetime

class DaylightSavings:
    def __init__(self, ds_end, ds_start):
        self.daylight_savings_end = datetime.strptime(ds_end, "%Y-%m-%d").date()
        self.daylight_savings_start = datetime.strptime(ds_start, "%Y-%m-%d").date()
     
    def getTimezone(self, date):
        datepart = date.date()
        if self.daylight_savings_end < datepart < self.daylight_savings_start :
            return "+12:00"
        return "+13:00"
