class UserClaim:
    def __init__(self, user, schedule):
        self.userName = user[1]
        self.pd_user_id = user[0]
        self.schedule_name=schedule.schedule_rate.schedule_name
        self.totalScheduleAmount = 0
        self.holidaysWorked = []
        self.sumScheduleAmounts(schedule)
        self.collateHolidays(schedule)

    def sumScheduleAmounts(self, schedule):
        for daily_claim in schedule.daily_claims:
            if self.pd_user_id == daily_claim.pd_user_id:
                self.totalScheduleAmount += daily_claim.daily_claim_amount
    
    def collateHolidays(self, schedule):
        for daily_claim in schedule.daily_claims:
            if self.pd_user_id == daily_claim.pd_user_id and daily_claim.isHoliday:
                self.holidaysWorked.append(daily_claim.day)