import calendar
from localpackage.UserPayment import UserPayment
import os
import os
import requests

class FormatOutput:
    def __init__(self, userclaims, schedules, enddate):
        self.userclaims = userclaims
        self.schedules = schedules
        self.enddate = enddate
        self.userpayments = []
        self.summaryText = ""
    
    def process(self):
        self.combineSchedulePayments()
        self.summaryText += self.printWelcome()
        self.summaryText += self.printSchedules()
        self.summaryText += self.printTotalPayments()
        self.summaryText += self.printExit()
        self.printToConsole(self.summaryText)
        return self.summaryText

    def combineSchedulePayments(self):
        #uniqueusers
        for uu in self.userclaims.getUniqueUsers():
            totalUserPayment = 0
            numberHolsWorked = []
            for scfu in self.userclaims.summarisedClaimsForUser:
                if scfu.pd_user_id == uu[0]:
                    totalUserPayment += scfu.totalScheduleAmount
                    if len(scfu.holidaysWorked) > 0:
                        numberHolsWorked.extend(scfu.holidaysWorked)
            if totalUserPayment > 0 :
                uniqueHols = len(set(numberHolsWorked))
                self.userpayments.append(UserPayment(uu[1], totalUserPayment, uniqueHols))

    def printWelcome(self):
        return "Hi team, \n\nThe following people have on-call claims for this week's pay run, the fortnight ending " + calendar.day_name[self.enddate.weekday()] + ", " + self.enddate.strftime("%Y-%m-%d") +"\n\n"

    def printExit(self):
        return "\nThanks\n"

    def printSchedules(self):
        scheduleListString = "Schedules included:\n"
        for sched in self.schedules:
            scheduleListString += "    " + sched.schedule_rate.schedule_name +"\n"
        return scheduleListString + "\n"
    
    def printTotalPayments(self):
        paymentListString = ""
        for up in self.userpayments:
            line = up.userName.ljust(19) + (" $"+ str(up.totalPayment) + ".00").ljust(9)
            if up.holidayCount > 0:
                line = line + " and " + str(up.holidayCount) + " alternate day(s)"
            paymentListString += line + "\n"
        return paymentListString
    
    def printNewUsers(self):
        print()
        if len([x for x in self.userclaims if x.is_new_user == True]) > 0:
            print("Also the following users appear to be new on paid on-call schedules.")
            print("Please ensure their BYOD Mobile allowance is processed as a contract variation.")

        for uc in self.userclaims:
            if uc.is_new_user == True:
                print("     "+uc.name)

    def printToConsole(self, text):
        print(text)