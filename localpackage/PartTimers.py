class PartTimer:
    def __init__(self, PartTimer, PagerDutyID, DayOff):
        days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        self.user_name = PartTimer
        self.pd_id = PagerDutyID
        self.day_off = days.index(DayOff)

class PartTimers:
    def __init__(self, partTimers):
        self.list = []
        for part_timer in partTimers:
            self.list.append(PartTimer(**part_timer))