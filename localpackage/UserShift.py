from datetime import datetime
from datetime import timedelta
from time import timezone
from localpackage.UserShiftPiece import UserShiftPiece

class UserShift:

    def __init__(self, pd_schedule_id, name, pd_user_id, start, end):
        self.pd_schedule_id = pd_schedule_id
        self.name = name
        self.pd_user_id = pd_user_id
        self.shiftamount = 0
        self.alternatedays = []
        self.timezone = timezone
        self.startdate = self.format_date(start)
        self.enddate = self.format_date(end)  
        delta = self.enddate-self.startdate 
        self.days = [self.startdate + timedelta(days=i) for i in range(delta.days + 1)]
        self.shift_pieces = []
        self.midnight_splits(self.startdate, self.enddate)

    def midnight_splits(self, startdate, enddate):
        #Possible Midnight = startdate + time 23:59:59
        midnight = startdate.replace(hour=23, minute=59, second=59)
        if enddate < midnight :
            #this is the last piece (might also be the first and last)
            self.shift_pieces.append(UserShiftPiece(self.pd_schedule_id, self.name, self.pd_user_id, startdate, enddate))
        else:
            # There are at least 2 pieces, the bit before midnight
            self.shift_pieces.append(UserShiftPiece(self.pd_schedule_id, self.name, self.pd_user_id, startdate, midnight))
            # and one or more after 
            self.midnight_splits((startdate + timedelta(days=1)).replace(hour=0, minute=0, second=0), enddate)
    
    def getShiftPieces(self):
        return self.shift_pieces
        
    # if the start date of a shift is later than mid-day, shift that date to the following midnight
    def format_date(self, date_string):
        #strip the timezone portion
        return datetime.strptime(date_string[:-6], "%Y-%m-%dT%H:%M:%S")
    
