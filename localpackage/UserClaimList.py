from localpackage.UserClaim import UserClaim

class UserClaimList:
    def __init__(self, schedules):
        self.allSchedules = schedules
        self.summarisedClaimsForUser = []        
        self.allUsers = []
        self.buildUserList()

    def getUniqueUsers(self):
        return set(self.allUsers)
    
    def buildUserList(self):
        for schedule in self.allSchedules:
            self.allUsers.extend(schedule.all_schedule_users)
    
    def combineUserDailyClaims(self, user):
        for schedule in self.allSchedules:
            user_summarised_claim = UserClaim(user, schedule)
            if user_summarised_claim.totalScheduleAmount > 0 :
                self.summarisedClaimsForUser.append(user_summarised_claim)

    def combineAllUserClaims(self):
        for user in self.getUniqueUsers():
            self.combineUserDailyClaims(user)
                
    

