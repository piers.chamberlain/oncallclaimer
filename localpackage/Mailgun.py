import requests
import os

class Mailgun:
    def __init__(self, Domain, MessageText, EmailToList, EmailCcList):
        self.url = "https://api.mailgun.net/v3/" + Domain + "/messages"
        self.directList = EmailToList
        self.ccList = EmailCcList
        self.messageText = MessageText


    def send(self):
        resp = requests.post(
        self.url,
        auth=("api", os.environ.get('MAILGUN_API_KEY')),
        data={"from": "Automated On-Call Process <mailgun@internal.trademe.co.nz>",
              "to": self.directList,
              "cc": self.ccList,
              "subject": "Trade Me on-call claims for next payrun",
              "text": self.messageText})
        return "Mailgun returned status code: " + resp.status_code