from datetime import timedelta
from localpackage.DailyUserShiftDuration import DailyUserShiftDuration

class ScheduleHistory:
    def __init__(self, schedule_rate, days, holidays, part_timers):
        self.schedule_rate = schedule_rate
        self.all_schedule_users = []
        self.pd_shifts = []
        self.shift_pieces = []
        self.per_worker_daily_duration = []
        self.daily_claims = []
        self.days = days
        self.holidays = holidays
        self.part_timers = part_timers
    
    def getUniqueUsers(self):
        return set(self.all_schedule_users)

    def processScheduleShifts(self):
        self.sum_shift_pieces()
        self.determine_max_user()
        self.setPerDiam()
        self.recordHolidaysWorked()

    def partTimeWorkerDayOff(self, claim):
        for part_timer in self.part_timers.list:
            if claim.day.weekday() == part_timer.day_off and claim.pd_user_id == part_timer.pd_id :
                return True
        return False
    
    def setPerDiam(self):
        for dc in self.daily_claims:
            #Holidays and Fri,Sat,Sun are High Rates
            #Also Jan gets high rate on Wednesdays (his normal day off)
            if (    dc.day.weekday()>3 or 
                    dc.day.date() in self.holidays or 
                    self.partTimeWorkerDayOff(dc)     ):
                dc.daily_claim_amount = self.schedule_rate.rate_high
            else:
                dc.daily_claim_amount = self.schedule_rate.rate_low
    
    def recordHolidaysWorked(self):
        for dc in self.daily_claims:
            if dc.day.date() in self.holidays:
                dc.markHoliday()

    def sum_shift_pieces(self):
        for day in self.days:
            for uu in self.getUniqueUsers():
                dd=timedelta(0,0)
                userdayshift = []
                for sp in self.shift_pieces:
                    if (sp.piece_start_date.date() == day.date() and sp.pd_user_id == uu[0] ):
                        userdayshift.append(sp)
                for uds in userdayshift:
                    dd = dd + uds.piece_duration

                if dd > timedelta(0,0):
                    self.per_worker_daily_duration.append(DailyUserShiftDuration(self.schedule_rate.schedule_id, uu[1], uu[0], day, dd))

    def determine_max_user(self):
        #Keeps the worker that did the most hours in that 24 hr period
        for day in self.days:
            #for each unique user
            worker_day_duration = []
            for uu in self.getUniqueUsers():
                for pwdd in self.per_worker_daily_duration:
                    if pwdd.day.date() == day.date() and pwdd.pd_user_id == uu[0] :
                        worker_day_duration.append(pwdd)

            if len(worker_day_duration)>0:
                worker_day_duration.sort(key=lambda x: x.duration, reverse=True)
                self.daily_claims.append(worker_day_duration[0])
    
      


    



