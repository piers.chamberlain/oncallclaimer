from localpackage.ScheduleRate import ScheduleRate

class ScheduleConfig:
    def __init__(self, rates):
        self.rates = []
        for rate in rates:
            self.rates.append(ScheduleRate(**rate))
