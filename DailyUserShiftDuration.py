class DailyUserShiftDuration:

    def __init__(self, pd_schedule_id, name, pd_user_id, day, duration):
        self.name = name
        self.pd_user_id = pd_user_id
        self.pd_schedule_id = pd_schedule_id
        self.day = day
        self.duration = duration
        self.daily_claim_amount = 0
        self.isHoliday = False

    def set_daily_claim_amount(self, dailyrate):
        self.daily_claim_amount = dailyrate
    
    def markHoliday(self):
        self.isHoliday = True