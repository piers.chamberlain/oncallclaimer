from pdpyras import APISession
from datetime import datetime
from datetime import timedelta
import json
import argparse
from DaylightSavings import DaylightSavings
from PartTimers import PartTimers
from ScheduleHistory import ScheduleHistory
from ScheduleRate import ScheduleConfig
from UserShift import PdUserShift
from UserClaimList import UserClaimList
from ConsoleOutput import ConsoleOutput

# -----------------
# Read Args
parser = argparse.ArgumentParser()
parser.add_argument(
        '--enddate',
        type=lambda s: datetime.strptime(s, '%Y-%m-%d'),
)
args = parser.parse_args()
startdate = args.enddate + timedelta(days= -13)
historydate = startdate + timedelta(weeks = -13)
args.enddate += timedelta(hours=23,minutes=59)
# -----------------

# -----------------
# Configuration load
config = open('config.json')
configContent = json.load(config)
schedule_config = ScheduleConfig(configContent['schedules'])
part_timers = PartTimers(configContent['partTimers'])

holiday_dates = []
for holiday in configContent['holidays']:
     holiday_dates.append(datetime.strptime(holiday, "%Y-%m-%d").date())

api_token = configContent['pdApiReadOnlyToken']
session = APISession(api_token)

ds_helper = DaylightSavings(configContent['daylightSavingsEnd'],configContent['daylightSavingsStart'])
delta = args.enddate-startdate 
days = []
days.append(startdate)
days.extend([startdate + timedelta(days=i+1) for i in range(delta.days)])

# -----------------

# -----------------
# Iterate through the schedules, build a set of days, and which users get paid
usershiftpieces=[]
userclaims = []
scheduleList = []

for rate in schedule_config.rates:
    claim_processing_list = []
    sched = session.rget('/schedules/'+ rate.schedule_id, params={    
        'since' : startdate.strftime("%Y-%m-%dT%H:%M:%S"+ds_helper.getTimezone(startdate)), 
        'until' : args.enddate.strftime("%Y-%m-%dT%H:%M:%S"+ds_helper.getTimezone(args.enddate)) })
    
    thisSchedule = ScheduleHistory(rate, days, holiday_dates, part_timers)

    #Build a UserShift array - a User can have muliple shifts on one or more rosters in the period
    for entry in sched['final_schedule']['rendered_schedule_entries']:
        pd_user_shift = PdUserShift(sched['id'], entry['user']['summary'], entry['user']['id'], entry['start'], entry['end'])
        thisSchedule.all_schedule_users.append((entry['user']['id'], entry['user']['summary']))
        thisSchedule.pd_shifts.append(pd_user_shift)
        thisSchedule.shift_pieces.extend(pd_user_shift.getShiftPieces())

    thisSchedule.processScheduleShifts()
    scheduleList.append(thisSchedule)
# -----------------

# -----------------
# Summarise the amounts per user and per schedule
claim_list = UserClaimList(scheduleList)
# Sum claims
claim_list.combineAllUserClaims()

# -----------------

# -----------------=
# Summary / write to console at least temporarily
console = ConsoleOutput(claim_list, scheduleList, args.enddate)
# -----------------