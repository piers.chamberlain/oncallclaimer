class ScheduleRate:
    def __init__(self, Name, PagerDutyID, RateWeekend, RateWeekday):
        self.schedule_name=Name        
        self.schedule_id = PagerDutyID
        self.rate_high = RateWeekend
        self.rate_low = RateWeekday

class ScheduleConfig:
    def __init__(self, rates):
        self.rates = []
        for rate in rates:
            self.rates.append(ScheduleRate(**rate))
