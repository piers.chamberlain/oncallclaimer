Processing logic to read the PagerDuty API and calculate the on-call costs for the 4 paid CAT rosters,
perhaps even replacing the spreadsheet / manual method.
~~It also detects NEW people on paid on-call (no shifts in last 13 weeks previous to the window start) ~~
~~and prompts the people lead to initiate the BYOD process to get the allowance.~~

To Dev:
1. Clone this down
2. Grab any dependencies via pip install
3. Set GOOGLE_APPLICATION_CREDENTIALS env var - points to a local .gcp json file containing the trademe-oncall service account creds
4. Set the PAGERDUTY_API_TOKEN as env vars (see: https://developer.pagerduty.com/docs/ZG9jOjExMDI5NTUx-authentication )
4. Ensure that the config.json is correct re: Api Keys, Schedule Ids, Holiday Dates and Daylight Savings, Email Details.
    The config you clone down from here is just an example - it actually reads from https://storage.cloud.google.com/oncall-claimer/lastExecution.json
    This means to test you might have to overwrite the values or redirect elsewhere
5. cmd line syntax is "python main.py"
6. the output will write to the console 

Live Deployment:
In the "trademe-oncall" GCP project 
* Cloud Scheduler (Oncall-Claim-Schedule) publishes to the generate-claim-email topic
* Cloud Function (generate-claim-email) runs and triggers the execution logic
* Mailgun and PagerDuty API keys are stored in Secret Manager, passed to the function as env vars.

Execution Logic
 - on triggering it reads lastExecution.json to ensure that it's been more than 13 days since it last ran.
    This is a hack to deal with cron lacking a "fortnightly" option, it gets triggered weekly but skipped every 2nd time.
 - If it runs, then for each schedule 
    - get the shifts per user between end date and the start date (-14 days)
    - divide the shifts into smaller pieces aligned to midnight or handover as needed
    - work out which user worked the MOST on any day covered
    - create a daily claim array - 1 person per day 
    - apply the $$ logic - business rules are:
        - Mon-Thu are paid at the schedule low rate
        - Fri-Sun are paid at the schedule high rate
        - Public Hols are high rate any day of the week
        - Jan gets high rates on Wednesdays (his normal day off)
    - Identify what worked days give the person an alternative day off
    - Collate the daily shifts 
- Just before printing
    - combine the person's schedule amounts
    - deduplicate the holidays worked (in the case that a person was on multiple schdules for the same day)
- Writes to console, sends via Mailgun.

ToDos:
* ~~Fix the Daylight savings bug where a transition falls mid window~~ (Done)
* Detect NEW people on paid on-call (i.e. no shifts in last 13 weeks previous to the window start) and prompt the people lead to initiate the BYOD process to get the allowance.
* Unit tests (future planting day)
* ~~Post to a GCP cloud function on a fortnightly timer (look mum, no hands)!~~ (Done)
* Ensure Holidays / Closedown dates are updated in Dec 2022 - planning to check if there are future dates within the next 5 months. If not get somehelp to update this
* Ensure the daylight savings values are updated year to year, same approach should work as above

