class UserShiftPiece:

    def __init__(self, pd_schedule_id, name, pd_user_id, start, end):
        self.name = name
        self.pd_user_id = pd_user_id
        self.pd_schedule_id = pd_schedule_id
        self.piece_start_date = start
        self.piece_end_date = end
        self.piece_duration = self.piece_end_date - self.piece_start_date